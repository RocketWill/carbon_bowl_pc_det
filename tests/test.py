#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
BASE_DIR= os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, os.path.join(BASE_DIR, '3rdparty/carbon_bowl_det'))
import cv2
import rospy
import yaml
from cv_bridge import CvBridge
from carbon_bowl_det_ros.srv import CarbonBowlDet, CarbonBowlDetResponse
import carbon_bowl_det.visualizer as visualizer
from carbon_bowl_det.visualizer import Visualizer
from detectron2.data import DatasetCatalog, MetadataCatalog
import argparse

visualizer._KEYPOINT_THRESHOLD = 0.01
visualizer._KEYPOINT_RADIUS = 1
visualizer._KEYPOINT_LINEWIDTH = 1
visualizer._BOX_LINEWIDTH = 1

CV_BRIDEG = CvBridge()


def response_msg_preds(response):
    preds = {
        'scores': [],
        'pred_classes': [],
        'pred_keypoints': [],
    }
    for obj in response.objs:
        pts = [[p.x, p.y, p.score] for p in obj.pts]
        preds['scores'].append(obj.score)
        preds['pred_classes'].append(obj.cls_id)
        preds['pred_keypoints'].append(pts)
    return preds

def visualize_response(img, response):
    preds = response_msg_preds(response)
    print(response)

    metadata_file=os.path.join(BASE_DIR,'3rdparty/carbon_bowl_det/outputs/output_v3/configs/metadata.yaml')
    with open(metadata_file) as f:
        metadata = yaml.load(f)
    dataset_metadata = MetadataCatalog.get('test')
    dataset_metadata.set(**metadata)

    v = Visualizer(img[:, :, ::-1],
                metadata=dataset_metadata, 
                scale=1.0)
    v = v.overlay_instances(keypoints=preds['pred_keypoints'])
    img_write_path = os.path.join('test_vis.jpg')
    print('write to : {}'.format(img_write_path))
    cv2.imwrite(img_write_path, v.get_image()[:, :, ::-1])

def test_one_frame():

    carbon_bowl_det = rospy.ServiceProxy('carbon_bowl_det', CarbonBowlDet)
    img_path = os.path.join(
        BASE_DIR,'3rdparty/carbon_bowl_det/tests/test.png')
    img_arr = cv2.imread(img_path)

    # call service
    img = CV_BRIDEG.cv2_to_imgmsg(img_arr, encoding="passthrough")
    response = carbon_bowl_det(img)
    # visulize
    visualize_response(img_arr, response)


if __name__ == "__main__":
    test_one_frame()