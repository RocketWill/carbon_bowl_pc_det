#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
BASE_DIR= os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, os.path.join(BASE_DIR, '3rdparty/carbon_bowl_det'))
from typing import List
import numpy as np
import cv2
import rospy
from carbon_bowl_det_ros.msg import PolygonObject, Point
from carbon_bowl_det_ros.srv import CarbonBowlDet, CarbonBowlDetResponse
from cv_bridge import CvBridge
import traceback
from carbon_bowl_det.perdictor import Predictor
import utils



class ServiceNode:
    def __init__(self, server_name = 'carbon_bowl_det'):
        self.server_name = server_name

        model_ckpt_folder = os.path.join(
            BASE_DIR, '3rdparty/carbon_bowl_det/outputs/id-7')
        model_weight = os.path.join(model_ckpt_folder, 'model_0004999.pth')
        rospy.loginfo('load model from: {}'.format(model_weight))
        self.predictor = Predictor(
            model_cfg= os.path.join(model_ckpt_folder, 'configs', 'train.yaml'),
            model_params = model_weight,
            score_thresh_hold=0.8
        )

        self.cv_bridge = CvBridge()
        s = rospy.Service(server_name, CarbonBowlDet, self.callback)
        rospy.init_node(server_name)
        rospy.loginfo('start server {}'.format(server_name))

    def spin(self):
        rospy.spin()

    def callback(self, request):
        response = None
        try:
            cv_image = self.cv_bridge.imgmsg_to_cv2(request.img, desired_encoding="passthrough")
            preds = self.predictor(cv_image)
            response = CarbonBowlDetResponse(objs=utils.preds_to_polygons(preds))
        except Exception:
            rospy.logerr(traceback.format_exc())
        return response

def main():
    server = ServiceNode()
    server.spin()

if __name__ == "__main__":
    main()
    