#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
BASE_DIR= os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, os.path.join(BASE_DIR, '3rdparty/carbon_bowl_det'))
from typing import List
import numpy as np
import cv2
import rospy
from sensor_msgs.msg import PointCloud2, CompressedImage, Image
from carbon_bowl_det_ros.msg import PolygonObjects
from cv_bridge import CvBridge
import traceback
from carbon_bowl_det.perdictor import Predictor
import utils
import ros_numpy
import time

class Subscriber:
    def __init__(self):
    
        model_ckpt_folder = os.path.join(
            BASE_DIR, '/home1/chengyong/Workspace/carbon_bowl/output/id-18')
        model_weight = os.path.join(model_ckpt_folder, 'model_0003999.pth')
        rospy.loginfo('load model from: {}'.format(model_weight))
        self.predictor = Predictor(
            model_cfg= os.path.join(model_ckpt_folder, 'configs', 'train.yaml'),
            model_params = model_weight,
            score_thresh_hold=0.8
        )

        self.cv_bridge = CvBridge()
        self.sub = rospy.Subscriber("/camera_argus100/publish_point_cloud", PointCloud2, self.callback)
        self.pub = rospy.Publisher('/camera_argus100/carbon_bowl_det', PolygonObjects)
        self.pub_img = rospy.Publisher('/camera_argus100/carbon_bowl_det_img', CompressedImage)
        self.is_busy = False

    def spin(self):
        rospy.spin()

    def callback(self, pc2_msg):
        if self.is_busy:
            rospy.loginfo('yield to process image')
            return
        response = None
        try:
            self.is_busy = True
            xyz_array = ros_numpy.point_cloud2.pointcloud2_to_xyz_array(pc2_msg)
            image = utils.point_cloud_2_birdseye(xyz_array)
            preds = self.predictor(image)
            im = utils.draw(image, preds)
            self.pub_img.publish(self.cv_bridge.cv2_to_compressed_imgmsg(im))
            self.pub.publish(PolygonObjects(objs=utils.preds_to_polygons(preds), header=pc2_msg.header))
            self.is_busy = False
        except Exception:
            self.is_busy = False
            rospy.logerr(traceback.format_exc())
        return response


if __name__ == "__main__":
    rospy.init_node('carbon_bowl_det_subscriber', anonymous=True)

    sub = Subscriber()
    sub.spin()