from typing import List
from carbon_bowl_det_ros.msg import PolygonObject, Point
import numpy as np
import matplotlib.pyplot as plt
import cv2
import time

kpts_line_color = [(0, 0, 255), (0, 255, 0), (255, 0, 0)]

def retangle_to_polygon(retangle):
    x0, y0, x1, y1 = retangle
    return [[x0, y0], [x1, y0], [x1, y1], [x0, y1]]

def draw(im, preds):
    predictions = preds["instances"].to("cpu")
    keypoints = predictions.pred_keypoints if predictions.has("pred_keypoints") else None
    classes = predictions.pred_classes if predictions.has("pred_classes") else None
    for cls_id, kpts in zip(classes, keypoints):
        if(cls_id == 1):
            p = [[int(p[0]), int(p[1])] for p in kpts]
            for i in range(len(p)):
                if i < len(p) - 1:
                    im = cv2.line(im, (int(p[i][0]), int(p[i][1])), (int(p[i + 1][0]), int(p[i + 1][1])),
                                  kpts_line_color[i % len(kpts_line_color)], 2)
    return im


def preds_to_polygons(preds) -> List[PolygonObject]:
    predictions = preds["instances"].to("cpu")
    scores = predictions.scores if predictions.has("scores") else None
    boxes = predictions.pred_boxes if predictions.has("pred_boxes") else None
    classes = predictions.pred_classes if predictions.has("pred_classes") else None
    keypoints = predictions.pred_keypoints if predictions.has("pred_keypoints") else None

    objs = []
    for cls_id, score, box,  pts in zip(classes, scores, boxes, keypoints):
        if cls_id == 0: # carbon bowl
            retangle_pts = retangle_to_polygon(box)
            _pts = [ Point(x=int(p[0]), y=int(p[1]), score=score) for p in retangle_pts]

        elif cls_id == 1: # carbon slot
            point_cloud_coord = keypoints_to_point_cloud_coord(pts)
            _pts = [ Point(x=float(p[0]), y=float(p[1]), score=p[2]) for p in point_cloud_coord]
        else:
            raise ValueError('not support cls_id: {}'.format(cls_id))
        obj = PolygonObject(pts=_pts, cls_id=cls_id, score=score)
        objs.append(obj)
    return objs

def keypoints_to_point_cloud_coord(keypoints):
    side_range = (-0.5, 0.5)  # left-most to right-most
    fwd_range = (-1.5, 1.5)  # back-most to forward-most
    res = 0.0005

    x_img = keypoints[:, 0]
    y_img = keypoints[:, 1]
    scores = keypoints[:, 2]
    x1 = x_img + int(np.floor(side_range[0] / res))
    y1 = y_img - int(np.ceil(fwd_range[1] / res))
    x = -y1 * res
    y = -x1 * res
    res = list(zip(x, y, scores))
    res = [list(x) for x in res]
    return res

def fig2data(fig):
    """
    fig = plt.figure()
    image = fig2data(fig)
    @brief Convert a Matplotlib figure to a 4D numpy array with RGBA channels and return it
    @param fig a matplotlib figure
    @return a numpy 3D array of RGBA values
    """
    import PIL.Image as Image
    # draw the renderer
    fig.canvas.draw()

    # Get the RGBA buffer from the figure
    w, h = fig.canvas.get_width_height()
    buf = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8)
    buf.shape = (w, h, 3)

    # canvas.tostring_argb give pixmap in ARGB mode. Roll the ALPHA channel to have it in RGBA mode
    buf = np.roll(buf, 3, axis=2)
    image = Image.frombytes("RGB", (w, h), buf.tostring())
    image = np.asarray(image)
    return image


# ==============================================================================
#                                                                   SCALE_TO_255
# ==============================================================================
def scale_to_255(a, min, max, dtype=np.uint8):
    """ Scales an array of values from specified min, max range to 0-255
        Optionally specify the data type of the output (default is uint8)
    """
    return (((a - min) / float(max - min)) * 255).astype(dtype)


# ==============================================================================
#                                                         POINT_CLOUD_2_BIRDSEYE
# ==============================================================================
def point_cloud_2_birdseye(points,
                           res=0.0005,
                           side_range=(-0.5, 0.5),  # left-most to right-most
                           fwd_range = (-1.5, 1.5), # back-most to forward-most
                           height_range=(-2.1, 2.1),  # bottom-most to upper-most
                           ):
    """ Creates an 2D birds eye view representation of the point cloud data.

    Args:
        points:     (numpy array)
                    N rows of points data
                    Each point should be specified by at least 3 elements x,y,z
        res:        (float)
                    Desired resolution in metres to use. Each output pixel will
                    represent an square region res x res in size.
        side_range: (tuple of two floats)
                    (-left, right) in metres
                    left and right limits of rectangle to look at.
        fwd_range:  (tuple of two floats)
                    (-behind, front) in metres
                    back and front limits of rectangle to look at.
        height_range: (tuple of two floats)
                    (min, max) heights (in metres) relative to the origin.
                    All height values will be clipped to this min and max value,
                    such that anything below min will be truncated to min, and
                    the same for values above max.
    Returns:
        2D numpy array representing an image of the birds eye view.
    """
    # EXTRACT THE POINTS FOR EACH AXIS
    x_points = points[:, 0]
    y_points = points[:, 1]
    z_points = points[:, 2]

    # FILTER - To return only indices of points within desired cube
    # Three filters for: Front-to-back, side-to-side, and height ranges
    # Note left side is positive y axis in LIDAR coordinates
    f_filt = np.logical_and((x_points > fwd_range[0]), (x_points < fwd_range[1]))
    s_filt = np.logical_and((y_points > -side_range[1]), (y_points < -side_range[0]))
    filter = np.logical_and(f_filt, s_filt)
    indices = np.argwhere(filter).flatten()

    # KEEPERS
    x_points = x_points[indices]
    y_points = y_points[indices]
    z_points = z_points[indices]

    # CONVERT TO PIXEL POSITION VALUES - Based on resolution
    x_img = (-y_points / res).astype(np.int32)  # x axis is -y in LIDAR
    y_img = (-x_points / res).astype(np.int32)  # y axis is -x in LIDAR

    # SHIFT PIXELS TO HAVE MINIMUM BE (0,0)
    # floor & ceil used to prevent anything being rounded to below 0 after shift
    x_img -= int(np.floor(side_range[0] / res))
    y_img += int(np.ceil(fwd_range[1] / res))

    # CLIP HEIGHT VALUES - to between min and max heights
    pixel_values = np.clip(a=z_points,
                           a_min=height_range[0],
                           a_max=height_range[1])

    # RESCALE THE HEIGHT VALUES - to be between the range 0-255
    pixel_values = scale_to_255(pixel_values,
                                min=height_range[0],
                                max=height_range[1])

    x_max = 1 + int((side_range[1] - side_range[0]) / res)
    y_max = 1 + int((fwd_range[1] - fwd_range[0]) / res)

    cmap = "hsv"  # Color map to use
    dpi = 100  # Image resolution
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(x_max / dpi, y_max / dpi), dpi=dpi)
    ax.scatter(x_img, y_img, s=1, c=pixel_values, linewidths=0, alpha=1, cmap=cmap)
    ax.set_facecolor((0, 0, 0))  # Set regions with no points to black


    ax.axis('scaled')  # {equal, scaled}
    ax.xaxis.set_visible(False)  # Do not draw axis tick marks
    ax.yaxis.set_visible(False)  # Do not draw axis tick marks
    plt.xlim([0, x_max])  # prevent drawing empty space outside of horizontal FOV
    plt.ylim([0, y_max])  # prevent drawing empty space outside of vertical FOV
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0,
                         hspace=0, wspace=0)
    plt.margins(0, 0)
    plt.gca().xaxis.set_major_locator(plt.NullLocator())
    plt.gca().yaxis.set_major_locator(plt.NullLocator())
    image = fig2data(fig)
    return image
