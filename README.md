# Carbon bowl detection ROS package

## Python Requirements

- python3
- pytorch (v1.4)
- detectron2 (v0.1.1)

# Add to your ROS workspace
```shell
cd YOUR_WORKSPACE_FOLDER/src
git clone git@gitlab.com:industrialdetection/carbon_bowl_det_ros.git --recursive
```

## Compile
```shell
catkin build # or catkin_make
source devel/setup.bash
```

## Start service

```shell
rosrun carbon_bowl_det_ros service.py
```

## Run test

```shell
rosrun carbon_bowl_det_ros test.py
```