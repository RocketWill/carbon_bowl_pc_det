import glob
import os
import cv2
import yaml
import tqdm
from detectron2.structures import Instances
from carbon_bowl_det.perdictor import Predictor
import carbon_bowl_det.visualizer as visualizer
from carbon_bowl_det.visualizer import Visualizer
from detectron2.data import DatasetCatalog, MetadataCatalog
import argparse

visualizer._KEYPOINT_THRESHOLD = 0.1
visualizer._KEYPOINT_RADIUS = 1
visualizer._KEYPOINT_LINEWIDTH = 1
visualizer._BOX_LINEWIDTH = 1


def preds_filter_by_cls_id(predictions, cls_id, keypoints=True):
    _filter = (predictions.pred_classes==cls_id)
    if keypoints:
        instances = Instances(
                image_size=predictions.image_size,
                scores=predictions.scores[_filter],
                pred_boxes = predictions.pred_boxes[_filter],
                pred_classes=predictions.pred_classes[_filter],
                pred_keypoints=predictions.pred_keypoints[_filter],
            )
    else:
        instances = Instances(
                image_size=predictions.image_size,
                scores=predictions.scores[_filter],
                pred_boxes = predictions.pred_boxes[_filter],
                pred_classes=predictions.pred_classes[_filter],
            )
    return instances



def predict_folder(img_dir:str, model_folder:str, epoch: int, vis_output_dir:str):
    with open(os.path.join(model_folder, 'configs', 'metadata.yaml') ) as f:
        metadata = yaml.load(f)
    ckpt_name = 'model_{:07d}.pth'.format(epoch) if epoch else 'model_final.pth'
    predictor = Predictor(
        model_cfg= os.path.join(model_folder, 'configs', 'train.yaml'),
        model_params = os.path.join(model_folder, ckpt_name),
        score_thresh_hold=0.8
        )
    imgs = glob.glob(os.path.join(img_dir, '*.png'))
    dataset_metadata = MetadataCatalog.get('test')
    dataset_metadata.set(thing_classes=metadata['obj_names'], **metadata)
    for img_path in tqdm.tqdm(imgs):
        im = cv2.imread(img_path)
        outputs = predictor(im)
        preds = outputs["instances"].to("cpu")
        v = Visualizer(im[:, :, ::-1],
                    metadata=dataset_metadata, 
                    scale=1.0)

        # show bowl and slot
        bowl = preds_filter_by_cls_id(preds, cls_id=0, keypoints=False)
        v.draw_instance_predictions(bowl)
        slot = preds_filter_by_cls_id(preds, cls_id=1)
        v = v.draw_instance_predictions(slot)

        img_write_path = os.path.join(vis_output_dir, os.path.basename(img_path))
        cv2.imwrite(img_write_path, v.get_image()[:, :, ::-1])

if __name__ == "__main__":
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('img_dir', type=str)
    args_parser.add_argument('model_folder', type=str, help='model name same as trainner output folder.')
    args_parser.add_argument('vis_output_dir', type=str)
    args_parser.add_argument('--epoch', default=None,  type=int)
    # args_parser.add_argument('--show_keypoints', default=None,  type=lambda x: map(int, x.split(','))), help='need visulize keypoints cls nums, like: "0,1"')
    args = args_parser.parse_args()

    os.makedirs(args.vis_output_dir)
    print('predict output to folder: {}'.format(args.vis_output_dir))
    predict_folder(
        img_dir=args.img_dir, 
        model_folder=args.model_folder, 
        epoch=args.epoch,
        vis_output_dir=args.vis_output_dir,
        )