import cv2
import random
import os
from carbon_bowl_det.dataset import DetectronDatasetLoader
from detectron2.data import DatasetCatalog, MetadataCatalog
import carbon_bowl_det.visualizer as visualizer
from carbon_bowl_det.visualizer import Visualizer
import tqdm

visualizer._KEYPOINT_THRESHOLD = 0.01
visualizer._KEYPOINT_RADIUS = 1
visualizer._KEYPOINT_LINEWIDTH = 1
visualizer._BOX_LINEWIDTH = 1

def test():
    """
        test
    """
    # dataset_root = '/home/xuefeng/data/carbon_slot_samples/train'
    dataset_root = '/home/xuefeng/sdb1/data/carbon_bowl/20200330/train'

    image_dir = dataset_root
    anno_dir = dataset_root

    obj_names = ['bowl','cao']
    keypoint_names = ['p0', 'p1', 'p2', 'p3']
    keypoint_connection_rules = [('p0', 'p1', (255, 0, 0)),  # color: (r,g,b)
                                ('p1', 'p2', (0, 255, 0)),
                                ('p2', 'p3', (0, 0, 255))]


    dataset_name = 'carbon_bowl'

    dataset_loader = DetectronDatasetLoader(
        image_dir=image_dir, 
        anno_dir= anno_dir, 
        bbox_padding={'bowl': 0.1, 'cao': 1.0},
        img_hw=(1024, 1280),
        num_keypoints = len(keypoint_names)
        )

    DatasetCatalog.register(dataset_name, dataset_loader.get_frames)
    MetadataCatalog.get(dataset_name).set(
        thing_classes=obj_names,
        thing_colors = [(255,255,255)],
        stuff_colors=[(255,255,255)],
        keypoint_names=keypoint_names,
        keypoint_connection_rules=keypoint_connection_rules,
        keypoint_flip_map= [('p0', 'p3'), ('p1', 'p2')]
        )
    dataset_metadata = MetadataCatalog.get(dataset_name)

    frames = DatasetCatalog.get(dataset_name)
    print('got {} frames'.format(len(frames)))
    print('first frame: {}'.format(frames[0]))

    

    random_selected_frames = random.sample(frames, 20)
    output_dir = 'outputs/anno_vis7'
    os.makedirs(output_dir, exist_ok=False)

    for frame in tqdm.tqdm(random_selected_frames):
        file_path = frame["file_name"]
        img_name = os.path.basename(file_path)
        im = cv2.imread(file_path)
        visualizer = Visualizer(im[:, :, ::-1], metadata=dataset_metadata, scale=1)
        vis = visualizer.draw_dataset_dict(frame)
        vis_img = vis.get_image()[:, :, ::-1]

        cv2.imwrite(os.path.join(output_dir, img_name), vis_img)

if __name__ == "__main__":
    test()
    