import glob
import os
import cv2
import yaml
import tqdm
from carbon_slot_detection.perdictor import Predictor
import carbon_slot_detection.visualizer as visualizer
from carbon_slot_detection.visualizer import Visualizer
from detectron2.data import DatasetCatalog, MetadataCatalog
import argparse

visualizer._KEYPOINT_THRESHOLD = 0.01
visualizer._KEYPOINT_RADIUS = 1
visualizer._KEYPOINT_LINEWIDTH = 1
visualizer._BOX_LINEWIDTH = 1

def benchmark(model_name):
    with open('configs/metadata.yaml') as f:
        metadata = yaml.load(f)
    predictor = Predictor(
        model_cfg= os.path.join(model_name, 'configs', 'train.yaml'),
        model_params = os.path.join(model_name, 'model_final.pth'),
        gpu_id = 0,
        score_thresh_hold=0.8
        )
    dataset_metadata = MetadataCatalog.get('test')
    dataset_metadata.set(**metadata)
    im = cv2.imread('tests/test.png')
    outputs = predictor(im)
    print('input: {}'.format(im.shape))
    for i in tqdm.tqdm(range(10000)):
        outputs = predictor(im)
        outputs["instances"].to("cpu")

if __name__ == "__main__":
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('model_name', type=str, help='model name same as trainner output folder.')
    args = args_parser.parse_args()
    benchmark(model_name=args.model_name)