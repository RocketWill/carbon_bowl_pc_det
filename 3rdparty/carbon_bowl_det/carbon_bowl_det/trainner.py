"""
    train scripts
    author: zengxuefeng
"""

import os
import yaml
import argparse
import shutil
from detectron2.engine import DefaultTrainer
from detectron2.config import get_cfg
from detectron2.data import DatasetCatalog, MetadataCatalog
from carbon_bowl_det.dataset import DetectronDatasetLoader

class CarbonSlotTrainner:
    def __init__(self, train_dataset_loader, val_dataset_loader, dataset_metadata, 
            train_config:str, output_folder):
        self.train_dataset_loader = train_dataset_loader
        self.val_dataset_loader = val_dataset_loader
        self.metadata = dataset_metadata
        self.output_folder = output_folder
        self.cfg = get_cfg()
        self.cfg.merge_from_file(train_config)

        assert self.cfg.MODEL.ROI_HEADS.NUM_CLASSES == len(dataset_metadata['obj_names'])

        print('cfg.OUTPUT_DIR: {}->{}'.format(self.cfg.OUTPUT_DIR, self.output_folder))
        self.cfg.OUTPUT_DIR = self.output_folder

    def __call__(self):
        self.register_datasets()
        print('cfg.SOLVER.WEIGHT_DECAY={}'.format(self.cfg.SOLVER.WEIGHT_DECAY))
        trainer = DefaultTrainer(self.cfg) 
        trainer.resume_or_load(resume=False)
        trainer.train()

    def register_datasets(self):
        datalodaers = [('train', self.train_dataset_loader), ('val', self.val_dataset_loader)]
        for name, loader in datalodaers:
            DatasetCatalog.register(name, loader.get_frames)
            metadata = {k:self.metadata[k] for k in [
                'obj_names', 
                'keypoint_names', 
                'keypoint_connection_rules',
                'keypoint_flip_map',
                'thing_colors',
                'stuff_colors',
                ]}
            MetadataCatalog.get(name).set(**metadata)

def copy_configs(config_folder, output_folder):
    cmd = 'cp -r {} {}'.format(config_folder, os.path.join(output_folder, 'configs'))
    print(cmd)
    os.system(cmd)

def train(config_folder, output_folder):

    with open(os.path.join(config_folder, 'metadata.yaml')) as f:
        metadata = yaml.load(f)
    bbox_padding = metadata['bbox_padding']
    keypoint_names = metadata['keypoint_names']
    dataset_root = metadata['dataset_root']
    img_hw = metadata['img_hw']
    train_dataset_loader = DetectronDatasetLoader(
        image_dir=os.path.join(dataset_root, 'train'), 
        anno_dir= os.path.join(dataset_root, 'train'),
        bbox_padding=bbox_padding,
        img_hw=img_hw,
        num_keypoints = len(keypoint_names))
    val_dataset_loader = DetectronDatasetLoader(
        image_dir=os.path.join(dataset_root, 'val'), 
        anno_dir= os.path.join(dataset_root, 'val'),
        bbox_padding=bbox_padding,
        img_hw=img_hw,
        num_keypoints = len(keypoint_names))


    train_config = os.path.join(config_folder, 'train.yaml')
    trainer = CarbonSlotTrainner(
        train_dataset_loader, 
        val_dataset_loader, 
        metadata, 
        train_config, 
        output_folder)
    trainer()

if __name__ == "__main__":
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('config', type=str, help='config folder')
    args_parser.add_argument('output', type=str, help='output folder')
    args = args_parser.parse_args()

    os.makedirs(args.output, exist_ok=False)
    copy_configs(args.config, args.output)
    train(args.config, args.output)
    