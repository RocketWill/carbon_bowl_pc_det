from typing import Dict, List
import glob
import os
import json
import numpy as np
from detectron2.structures import BoxMode


def pad_rectangle(rectangle, padding):
    padding = float(padding)
    w = rectangle[2] - rectangle[0]
    h = rectangle[3] - rectangle[1]
    pad_w = w * padding
    pad_h = h * padding
    new_rectangle = [
        int(rectangle[0]-pad_w/2.0), 
        int(rectangle[1]-pad_h/2.0), 
        int(rectangle[2]+pad_w/2.0), 
        int(rectangle[3]+pad_h/2.0), 
    ]
    return new_rectangle

def get_polygon_rectangle(polygon):
    """
        get polygon bouning rectangle
    """
    pts = np.array(polygon)
    rectangle = [pts[:, 0].min(), pts[:, 1].min(), pts[:, 0].max(), pts[:, 1].max()]
    return rectangle

def get_keypoints(pts):
    visibility = 2
    pts_new = []
    for x, y in pts:
        pts_new+= [x, y, visibility]
    return pts_new

class LShape:
    """
        labelme shape
    """
    def __init__(self, content: Dict):
        self.points = content['points']

class LRectangle(LShape):
    def __init__(self, content: Dict):
        super(LRectangle, self).__init__(content)

    def get_bbox(self, pad) -> List[int]:
        # [[x0,y0], [x1,y0]] -> [x0,y0,x1,y0]
        rect = self.points[0] + self.points[1] 
        return pad_rectangle(rect, pad)

class LPolygon(LShape):
    def __init__(self, content: Dict):
        super(LPolygon, self).__init__(content)

    def get_bbox(self, pad) -> List[int]:
        bbox = get_polygon_rectangle(self.points)
        bbox = pad_rectangle(bbox, pad)
        return bbox

    def get_keypoints(self, visibility):
        visibility = 2
        pts_new = []
        for x, y in self.points:
            pts_new+= [x, y, visibility]
        return pts_new



def parse_labelme_polygon(shape: Dict, bbox_padding: float, num_keypoints: int) -> Dict:
    anno = None
    polygon = shape['points']
    bbox = get_polygon_rectangle(polygon)
    bbox = pad_rectangle(bbox, bbox_padding)
    if len(shape['points']) == num_keypoints:
        keypoints = get_keypoints(shape['points'])
        anno = {
            'bbox': bbox,
            'bbox_mode': BoxMode.XYXY_ABS,
            'iscrowd': 0,
            'keypoints': keypoints
            }
    else:
        print("len(shape['points']) != num_keypoints,\
             which is {} vs {}, will skip it.".format(
                 len(shape['points']), num_keypoints))

    return anno

def parse_labelme_retangle(shape: Dict, bbox_padding: float, num_keypoints: int) -> Dict:
    anno = None
    bbox = shape['points'][0] + shape['points'][1] # [[x0,y0], [x1,y1]] -> [x0,y0,x1,y1]
    bbox = pad_rectangle(bbox, bbox_padding)
    if len(shape['points']) == num_keypoints:
        keypoints = get_keypoints(shape['points'])
        anno = {
            'bbox': bbox,
            'bbox_mode': BoxMode.XYXY_ABS,
            'iscrowd': 0,
            'keypoints': keypoints
            }

    return anno

def parse_label_shape(content: Dict):
    _type = content['shape_type']
    if _type == 'polygon':
        shape = LPolygon(content)
    elif _type == 'rectangle':
        shape = LRectangle(content)
    else:
        raise NotImplementedError
    return shape

def get_one_anno(shape_content, bbox_padding, num_keypoints):
    label_name = shape_content['label']
    if label_name == 'bowl':
        rect = parse_label_shape(shape_content)
        assert isinstance(rect, LRectangle)
        bbox = rect.get_bbox(bbox_padding[label_name])
        kpts = [0,0,0] * num_keypoints
        cls_id = 0
    elif label_name == 'cao':
        polygon = parse_label_shape(shape_content)
        assert isinstance(polygon, LPolygon)
        bbox = polygon.get_bbox(bbox_padding[label_name])
        kpts = polygon.get_keypoints(visibility=2)
        cls_id = 1
    else:
        raise NotImplementedError

    anno = {
        'bbox': bbox,
        'bbox_mode': BoxMode.XYXY_ABS,
        'category_id': cls_id,
        'iscrowd': 0,
        'keypoints': kpts
        }
        
    return anno


class DetectronDatasetLoader:
    def __init__(self, image_dir, anno_dir, bbox_padding, img_hw, num_keypoints):
        self.ori_annos = self.load_labelme_annos_from_folder(anno_dir)
        assert len(self.ori_annos) != 0
        self.image_dir = image_dir
        self.bbox_padding = bbox_padding
        self.img_hw = img_hw
        self.num_keypoints=num_keypoints

    def load_labelme_annos_from_folder(self, dataset_root):
        """
            return: [ori_anno, ...]
        """
        ori_annos = []
        json_files = glob.glob(os.path.join(dataset_root, '*.json'))
        for json_file in json_files:
            with open(json_file) as f:
                ori_anno = json.load(f)
            ori_annos.append(ori_anno)
        return ori_annos

    def parse_labelme_annos(self):
        frames = []
        for ori_frame_anno in self.ori_annos:
            labelme_shapes = ori_frame_anno['shapes']
            annos = []
            for shape in labelme_shapes:
                anno = get_one_anno(shape, self.bbox_padding, self.num_keypoints)
                if anno:
                    annos.append(anno)
                else:
                    print('file: {}, shape.label: {}'.format(ori_frame_anno['imagePath'], shape['label']))
                
            img_path = ori_frame_anno['imagePath']

            frame={ 
                'annotations': annos,
                'file_name': os.path.join(self.image_dir, img_path),
                'height': self.img_hw[0],
                'width': self.img_hw[1]
                }
            frames.append(frame)
        return frames
    
    def get_frames(self, name='undefined_name'):
        frames = self.parse_labelme_annos()
        return frames