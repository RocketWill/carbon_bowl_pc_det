"""
"""
import os
import detectron2
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg

class Predictor(DefaultPredictor):
    def __init__(self, model_cfg, model_params, score_thresh_hold=0.2):
        cfg = get_cfg()
        cfg.merge_from_file(model_cfg)
        cfg.MODEL.WEIGHTS = model_params
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = score_thresh_hold
        super(Predictor, self).__init__(cfg)