dataset_name=val
img_folder=/home/xuefeng/sdb1/data/carbon_bowl/20200330/${dataset_name}
model_folder=outputs/id-7
epoch=4999
output_folder=${model_folder}/predict_vis_${dataset_name}_ep_${epoch}
python tests/test_predictor.py ${img_folder} ${model_folder} ${output_folder} --epoch ${epoch} 